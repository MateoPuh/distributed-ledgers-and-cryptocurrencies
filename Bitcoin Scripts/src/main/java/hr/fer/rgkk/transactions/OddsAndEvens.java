package hr.fer.rgkk.transactions;

import org.bitcoinj.core.*;
import org.bitcoinj.crypto.TransactionSignature;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import java.security.SecureRandom;

import static org.bitcoinj.script.ScriptOpCodes.*;

/**
 * You must implement Odds and Evens game between two players such that only
 * the winner can unlock the coins.
 */
public class OddsAndEvens extends ScriptTransaction {

    // Alice's private key
    private final ECKey aliceKey;
    // Alice's nonce
    private final byte[] aliceNonce;
    // Bob's private key
    private final ECKey bobKey;
    // Bob's nonce
    private final byte[] bobNonce;

    // Key used in unlocking script to select winning player.
    // Winner is preselected here so that we can run tests.
    private final ECKey winningPlayerKey;

    private OddsAndEvens(
            WalletKit walletKit, NetworkParameters parameters,
            ECKey aliceKey, byte[] aliceNonce,
            ECKey bobKey, byte[] bobNonce,
            ECKey winningPlayerKey
    ) {
        super(walletKit, parameters);
        this.aliceKey = aliceKey;
        this.aliceNonce = aliceNonce;
        this.bobKey = bobKey;
        this.bobNonce = bobNonce;
        this.winningPlayerKey = winningPlayerKey;
    }

    @Override
    public Script createLockingScript() {
        byte[] ca = Utils.sha256hash160(aliceNonce);
        byte[] cb = Utils.sha256hash160(bobNonce);

        return new ScriptBuilder()
                .op(OP_2DUP) // duplicate RA and RB
                .op(OP_HASH160) // calculate HASH160(RA)
                .data(ca)
                .op(OP_EQUALVERIFY) // compare CA and HASH160(RA)
                .op(OP_HASH160) // calculate HASH160(RB)
                .data(cb)
                .op(OP_EQUALVERIFY) // compare CB and HASH160(RB)
                .op(OP_SIZE) // RA size
                .op(OP_NIP)
                .number(16)
                .op(OP_SUB) // NA = SIZE(RA) - 16
                .op(OP_SWAP)
                .op(OP_SIZE) // RB size
                .op(OP_NIP)
                .number(16)
                .op(OP_SUB) // NB = SIZE(RB) - 16
                .op(OP_EQUAL) // if NA and NB are equal "Even" player is winner
                .op(OP_IF)
                .data(aliceKey.getPubKey())
                .op(OP_CHECKSIG) // if NA and NB are equal Alice is winner
                .op(OP_ELSE)
                .data(bobKey.getPubKey())
                .op(OP_CHECKSIG) // if NA and NB are different Bob is winner
                .op(OP_ENDIF)
                .build();
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedTransaction) {
        TransactionSignature signature = sign(unsignedTransaction, winningPlayerKey);
        return new ScriptBuilder()
                .data(signature.encodeToBitcoin())
                .data(bobNonce)   // Odds player
                .data(aliceNonce) // Evens player
                .build();
    }

    public static OddsAndEvens of(
            WalletKit walletKit, NetworkParameters parameters,
            OddsEvenChoice aliceChoice, OddsEvenChoice bobChoice,
            WinningPlayer winningPlayer
    ) {
        byte[] aliceNonce = randomBytes(16 + aliceChoice.value);
        byte[] bobNonce = randomBytes(16 + bobChoice.value);

        ECKey aliceKey = randKey();
        ECKey bobKey = randKey();

        // Alice is EVEN, bob is ODD
        ECKey winningPlayerKey = WinningPlayer.EVEN == winningPlayer ? aliceKey : bobKey;

        return new OddsAndEvens(
                walletKit, parameters,
                aliceKey, aliceNonce,
                bobKey, bobNonce,
                winningPlayerKey
        );
    }

    private static byte[] randomBytes(int length) {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[length];
        random.nextBytes(bytes);
        return bytes;
    }

    public enum WinningPlayer {
        ODD, EVEN
    }

    public enum OddsEvenChoice {

        ZERO(0),
        ONE(1);

        public final int value;

        OddsEvenChoice(int value) {
            this.value = value;
        }
    }
}

