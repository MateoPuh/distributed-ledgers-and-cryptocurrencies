package hr.fer.rgkk.transactions;

import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import static org.bitcoinj.script.ScriptOpCodes.*;

/**
 * You must implement locking and unlocking script such that transaction output is locked by 2 integers x and y
 * such that they are solution to the equation system:
 * <pre>
 *     x + y = first four digits of your student id
 *     abs(x-y) = last four digits of your student id
 * </pre>
 * If needed change last digit of your student id such that x and y have same parity. This is needed so that equation
 * system has integer solutions.
 */
public class LinearEquationTransaction extends ScriptTransaction {

    // x + y = 3650
    // |x - y| = 2998
    private final int x = 3324;
    private final int y = 326;

    private final Script redeemScript;

    public LinearEquationTransaction(WalletKit walletKit, NetworkParameters parameters) {
        super(walletKit, parameters);
        redeemScript = new ScriptBuilder()
                .op(OP_2DUP)
                .op(OP_ADD)
                .number(3650)
                .op(OP_EQUAL)
                .op(OP_VERIFY)
                .op(OP_SUB)
                .op(OP_ABS)
                .number(2998)
                .op(OP_EQUAL)
                .build(); // Build "OP_2DUP OP_ADD 3650 OP_EQUAL OP_VERIFY OP_SUB OP_ABS 2998 OP_EQUAL" redeem script
    }

    @Override
    public Script createLockingScript() {
        return ScriptBuilder.createP2SHOutputScript(redeemScript);
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedScript) {
        return new ScriptBuilder()
                .number(x)
                .number(y)
                .data(redeemScript.getProgram())
                .build();
    }
}
