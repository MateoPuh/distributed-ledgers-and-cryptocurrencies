package hr.fer.rgkk.transactions;

import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.crypto.TransactionSignature;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import static org.bitcoinj.script.ScriptOpCodes.*;

/**
 * You must implement standard Pay-2-Public-Key-Hash transaction type.
 */
public class PayToPubKeyHash extends ScriptTransaction {

    private final ECKey key;

    public PayToPubKeyHash(WalletKit walletKit, NetworkParameters parameters) {
        super(walletKit, parameters);
        key = getWallet().freshReceiveKey();
    }

    @Override
    public Script createLockingScript() {
        return new ScriptBuilder()     // Create new ScriptBuilder object that builds locking script
                .op(OP_DUP)       // Add OP_DUP to the locking script
                .op(OP_HASH160)       // Add OP_HASH160 to the locking script
                .data(key.getPubKeyHash())       // Add public key hash to the locking script
                .op(OP_EQUALVERIFY)       // Add OP_EQUALVERIFY to the locking script
                .op(OP_CHECKSIG)       // Add OP_CHECKSIG to the locking script
                .build();              // Build "OP_DUP OP_HASH160 <pubKeyHash> OP_EQUALVERIFY OP_CHECKSIG" locking script
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedTransaction) {
        TransactionSignature txSig = sign(unsignedTransaction, key); // Create key signature
        return new ScriptBuilder()                                   // Create new ScriptBuilder
                .data(txSig.encodeToBitcoin())                       // Add key signature to unlocking script
                .data(key.getPubKey())                               // Add public key to the unlocking script
                .build();                                            // Build "<signature(key)> <pubKey>" unlocking script
    }
}
